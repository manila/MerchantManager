package junit_tests;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;

public class HelloWorldEndPointTest {
	
	@Test
	public void get_request_hello() {		
		Client client = ClientBuilder.newClient();		
		WebTarget webTarget = client.target("http://02267-manila.compute.dtu.dk:8003");
		webTarget = webTarget.path("hello");
		
		Response response = webTarget.request().get();		
		assertEquals(200, response.getStatus());
		assertEquals("Hello from Al", response.readEntity(String.class));
	}	

}
