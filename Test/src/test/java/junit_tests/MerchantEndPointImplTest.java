package junit_tests;

import static org.junit.Assert.assertEquals;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;

import resources.MerchantResource;

public class MerchantEndPointImplTest {
	private WebTarget webTarget;
	private final String CPR_NUMBER = "9284429999";
	private final String MERCHANTS_PATH = "merchants";
	private final String PORT = ":8003";
	private final String TARGET = "http://02267-manila.compute.dtu.dk" + PORT;

	@Before
	public void setUp() {
		Client client = ClientBuilder.newClient();
		webTarget = client.target(TARGET);
	}

	@Test
	public void givenMerchantResourcePostWithCprNumber_returnPostWasSuccessful() {
		MerchantResource merchantResource = new MerchantResource();
		merchantResource.cprNumber = CPR_NUMBER;

		webTarget = webTarget.path(MERCHANTS_PATH);

		Response response = webTarget.request().post(Entity.entity(merchantResource, MediaType.APPLICATION_JSON));

		assertEquals(200, response.getStatus());
		response.close();
	}

}
