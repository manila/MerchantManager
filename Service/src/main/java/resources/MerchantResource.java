package resources;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * A resource used to receive information of a merchant from a REST interface
 *
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 */
@XmlRootElement()
public class MerchantResource {
	@XmlElement public String cprNumber;
}
