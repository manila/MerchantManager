package dk.manila.merchantmanager.merchantservice;

/**
 * Interface representing a merchant service
 *
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 */
public interface MerchantService {
	/**
	 * Registers a merchant in the MerchantService given his/her CPR number.
	 * 
	 * @param cpr
     *     allowed object is
     *     {@link String }
	 */
	void registerMerchant(String cpr);

	/**
	 * Returns true if this MerchantsService contains a merchant with the specified cpr.
	 * 
	 * @param merchantCpr
     *     allowed object is
     *     {@link String }
	 * @return
     *     possible object is
     *     {@link boolean }
	 */
	boolean contains(String merchantCpr);
}
