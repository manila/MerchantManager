package dk.manila.merchantmanager.merchantservice;

import java.util.concurrent.CompletableFuture;

import dk.manila.merchantmanager.queue.Event;
import dk.manila.merchantmanager.queue.EventReceiver;
import dk.manila.merchantmanager.queue.EventSender;
import dk.manila.merchantmanager.repository.MerchantRepository;
import dk.manila.merchantmanager.tools.CprNumber;
import dk.manila.merchantmanager.tools.Merchant;

/**
 * Implementation of a MerchantService
 *
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 */
public class MerchantServiceImpl implements MerchantService, EventReceiver {
	private MerchantRepository merchantRepository;
	private EventSender eventSender;
	private CompletableFuture<Event> future;

	/**
	 * Constructor of the class MerchantServiceImpl
	 * 
	 * @param eventSender
     *     allowed object is
     *     {@link EventSender }
	 * @param merchantRepository,
     *     allowed object is
     *     {@link MerchantRepository }
	 */
	public MerchantServiceImpl(EventSender eventSender, MerchantRepository merchantRepository) {
		this.merchantRepository = merchantRepository;
		this.eventSender = eventSender;
	}

	/**
	 * Sets a new MerchantRepository used for testing purposed
	 * 
	 * @param repository
     *     allowed object is
     *     {@link MerchantRepository }
	 */
	protected void setMerchantRepository(MerchantRepository repository) {
		this.merchantRepository = repository;
	}

	@Override
	public void registerMerchant(String cpr) {
		CprNumber cprNumber = new CprNumber(cpr);
		Merchant merchant = new Merchant(cprNumber);
		merchantRepository.addMerchant(merchant);
	}

	@Override
	public boolean contains(String merchantCpr) {
		return merchantRepository.contains(new Merchant(new CprNumber(merchantCpr)));
	}

	@Override
	public void receiveEvent(Event event) {
		// handle event
	}

}
