package dk.manila.merchantmanager.rest;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import dk.manila.merchantmanager.merchantservice.MerchantService;
import dk.manila.merchantmanager.merchantservice.MerchantServiceImpl;
import dk.manila.merchantmanager.queue.EventReceiver;
import dk.manila.merchantmanager.queue.RabbitMqListener;
import dk.manila.merchantmanager.queue.RabbitMqSender;
import dk.manila.merchantmanager.repository.MerchantRepositoryImpl;

/**
 * Application that starts a REST interface, creates a MerchantService and
 * starts the listener of queue.
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 *
 */
@ApplicationPath("/")
public class RestApplication extends Application {
	private final String EXCHANGE_NAME = "dtu_pay_exchange";
	private final String EXCHANGE_HOME = "rabbitmq";
	private final String EXCHANGE_TYPE = "fanout";
	private MerchantService service;

	/**
	 * Constructor of RestApplication in which we start a REST interface, create a
	 * MerchantService {@link MerchantService} and start the listener of queue.
	 */
	public RestApplication() {
		service = new MerchantServiceImpl(new RabbitMqSender(EXCHANGE_NAME, EXCHANGE_HOME, EXCHANGE_TYPE),
				new MerchantRepositoryImpl());
		try {
			new RabbitMqListener((EventReceiver) service).listen(EXCHANGE_NAME, EXCHANGE_HOME, EXCHANGE_TYPE);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Map<String, Object> getProperties() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("service", service);
		return map;
	}
}
