package dk.manila.merchantmanager.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dk.manila.merchantmanager.merchantservice.MerchantService;
import resources.MerchantResource;

/**
 * Endpoint of a Merchant Manager
 *
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 */
@Path("/merchants")
public class MerchantEndpointImpl {
	private MerchantService service;
	private final String SERVICE_KEY = "service";

	/**
	 * 
	 * Constructor of a MerchantEndpointImpl
	 * 
	 * @param app
     *     allowed object is
     *     {@link String }
	 */
	public MerchantEndpointImpl(@Context Application app) {
		service = (MerchantService) app.getProperties().get(SERVICE_KEY);
	}
	
	/**
	 * 
	 * Calls the MerchantService to register a merchant coming from a REST request.
	 * 
	 * @param merchant
     *     allowed object is
     *     {@link MerchantResource }
	 * @return
     *     possible object is
     *     {@link Response }
	 */
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createMerchant(MerchantResource merchant) {
		service.registerMerchant(merchant.cprNumber);
		return Response.ok().build();
	}
	
}
