package dk.manila.merchantmanager.tools;

/**
 * Representation of Merchant
 *
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 */
public final class Merchant {
	
	private final CprNumber cprNumber;

	/**
	 * Constructor of the class Merchant
	 * 
	 * @param cpr
	 *     allowed object is
     *     {@link String }
	 */
	public Merchant(CprNumber cpr) {		
		this.cprNumber = cpr;
	}

	/**
	 * Gets the CPR number of the merchant
	 *
	 * @return
     *     possible object is
     *     {@link String }
	 */
	public CprNumber getCprNumber() {
		return cprNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cprNumber == null) ? 0 : cprNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Merchant other = (Merchant) obj;
		if (cprNumber == null) {
			if (other.cprNumber != null)
				return false;
		} else if (!cprNumber.equals(other.cprNumber))
			return false;
		return true;
	}

}
