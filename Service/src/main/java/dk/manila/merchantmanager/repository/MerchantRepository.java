package dk.manila.merchantmanager.repository;

import dk.manila.merchantmanager.tools.Merchant;

/**
 * A repository of merchants
 *
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 */
public interface MerchantRepository {
	/**
	 * 
	 * Adds a merchant to the repository.
	 * 
	 * @param merchant
     *     allowed object is
     *     {@link Merchant }
	 */
	void addMerchant(Merchant merchant);
	/**
	 * 
	 * Returns true if the repository contains the specified merchant.
	 * 
	 * @param merchant
     *     allowed object is
     *     {@link Merchant }
	 * @return
     *     possible object is
     *     {@link boolean }
	 */
	boolean contains(Merchant merchant);
}
