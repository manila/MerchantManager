package dk.manila.merchantmanager.repository;

import java.util.HashSet;
import java.util.Set;

import dk.manila.merchantmanager.tools.Merchant;

/**
 * Implementation of a MerchantRepository
 *
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 */
public class MerchantRepositoryImpl implements MerchantRepository {

	public MerchantRepositoryImpl() {
	}

	private Set<Merchant> merchants = new HashSet<>();

	@Override
	public void addMerchant(Merchant merchant) {
		if (merchant == null)
			throw new NullPointerException();
		merchants.add(merchant);
	}

	@Override
	public boolean contains(Merchant merchant) {
		for (Merchant m : merchants)
			if (m.getCprNumber().equals(merchant.getCprNumber()))
				return true;
		return false;
	}

	/**
	 * Returns a set of merchants for testing purposes.
	 * 
	 * @return
	 * 
     *     possible object is
     *     {@link Set<Merchant> }
	 */
	protected Set<Merchant> getMerchants() {
		return new HashSet<>(merchants);
	}

	/**
	 * Sets a set of merchants in the MerchantRepositoryImpl for testing purposes.
	 * 
	 * @param merchants
     *     allowed object is
     *     {@link Set<Merchant> }
	 */
	protected void setMerchants(Set<Merchant> merchants) {
		this.merchants = new HashSet<>(merchants);
	}
}
