package dk.manila.merchantmanager.queue;

/**
 * A receiver of events from a queue
 *
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 */
public interface EventReceiver {
	/**
	 * 
	 * Makes an action corresponding to a specified event.
	 * 
	 * @param event
     *     allowed object is
     *     {@link Event }
	 */
	void receiveEvent(Event event);
}
