package dk.manila.merchantmanager.queue;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

/**
 * A listener of a Rabbit Mq queue
 *
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 */
public class RabbitMqListener {
	private EventReceiver eventReceiver;

	/**
	 * Constructor of the class RabbitMqListener
	 * 
	 * @param eventReceiver
     *     allowed object is
     *     {@link EventReceiver }
	 */
	public RabbitMqListener(EventReceiver eventReceiver) {
		this.eventReceiver = eventReceiver;
	}

	/**
	 * Listens to a Rabbit Mq queue and sends the events to the EventReceiver
	 * 
	 * @param exchangeName
     *     allowed object is
     *     {@link String }
	 * @param exchangeHost
     *     allowed object is
     *     {@link String }
	 * @param exchangType
     *     allowed object is
     *     {@link String }
	 * @throws IOException
	 * 				if the connection to the queue cannot be established
	 * @throws TimeoutException
	 * 				if it takes to much time to send the request
	 */
	public void listen(String exchangeName, String exchangeHost, String exchangType)
			throws IOException, TimeoutException {
		ConnectionFactory connectionFactory = new ConnectionFactory();
		connectionFactory.setHost(exchangeHost);
		Connection connection = connectionFactory.newConnection();
		Channel channel = connection.createChannel();
		channel.exchangeDeclare(exchangeName, exchangType);
		channel.queueDeclare(exchangeName, false, false, false, null);

		String queueName = channel.queueDeclare().getQueue();
		channel.queueBind(queueName, exchangeName, "");

		DeliverCallback deliverCallback = (consumerTag, delivery) -> {

			String message = new String(delivery.getBody());
			Event event = new Gson().fromJson(message, Event.class);

			eventReceiver.receiveEvent(event);
		};

		channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {
		});
	}
}
