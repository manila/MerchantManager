package dk.manila.merchantmanager.queue;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * A sender of events through a Rabbit Mq queue
 *
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 */
public class RabbitMqSender implements EventSender {
	private String exchangeName;
	private String exchangeHost;
	private String exchangeType;
	/**
	 * Constructor of the class RabbitMqSender
	 * 
	 * @param exchangeName
     *     allowed object is
     *     {@link String }
	 * @param exchangeHost
     *     allowed object is
     *     {@link String }
	 * @param exchangeType
     *     allowed object is
     *     {@link String }
	 */
	public RabbitMqSender(String exchangeName, String exchangeHost, String exchangeType) {
		this.exchangeName = exchangeName;
		this.exchangeHost = exchangeHost;
		this.exchangeType = exchangeType;
	}	
	@Override
	public void sendEvent(Event event) throws IOException, TimeoutException {
		String message = new Gson().toJson(event);
		ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(exchangeHost);
        Connection connection = connectionFactory.newConnection();
		Channel channel = connection.createChannel();
		
		channel.exchangeDeclare(exchangeName, exchangeType);
		
		channel.basicPublish(exchangeName, "", null, message.getBytes());
	}
}
