package dk.manila.merchantmanager.queue;

import java.util.List;

/**
 * Class representing an event
 *
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 */
public class Event {

	private String instruction;
	private List<Object> args;
	
	/**
	 * 
	 * Constructor of an Event
	 * 
	 * @param instruction
     *     allowed object is
     *     {@link String }
     *     
	 *	   instruction representing what the receiver should do with this Event
	 * 
	 * @param args
     *     allowed object is
     *     {@link List<Object> }
     *     
     *     arguments we want to send in order to fulfilled the instruction
	 * 
	 */
	public Event(String instruction, List<Object> args) {
		this.instruction = instruction;
		this.args = args;
	}
	
	/**
	 *  Returns the instruction enclosed in the Event
	 *  
	 * @return
     *     possible object is
     *     {@link String}
	 */
	public String getInstruction() {
		return this.instruction;
	}
	
	/**
	 *  Returns the arguments enclosed in the Event
	 * 
	 * @return
     *     possible object is
     *     {@link List<Object>}
	 */
	public List<Object> getArgs() {
		return this.args;
	}
	
}
