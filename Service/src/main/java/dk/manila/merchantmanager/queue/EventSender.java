package dk.manila.merchantmanager.queue;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * A sender of events to a queue
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 *
 */
public interface EventSender {
	
	/**
	 * Sends an event to a queue.
	 * 
	 * @param event
     *     allowed object is
     *     {@link Event }
	 * @throws IOException
	 * 				if the connection to the queue cannot be established
	 * @throws TimeoutException
	 * 				if it takes to much time to send the request
	 */
	public void sendEvent(Event event) throws IOException, TimeoutException;

}
