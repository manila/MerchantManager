package dk.manila.merchantmanager.repository;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import dk.manila.merchantmanager.tools.CprNumber;
import dk.manila.merchantmanager.tools.Merchant;

/**
 * 
 * @author Kaloyan, Khushboo
 *
 */
public class MerchantRepositoryTest {
	private MerchantRepositoryImpl repository;
	private Merchant merchant;
	@Before
	public void initialization() {
		repository = new MerchantRepositoryImpl();
		merchant = new Merchant(new CprNumber("1234"));
	}
	@Test(expected=NullPointerException.class)
	public void givenNullMerchantTryToRegister_returnNullPointerException() {
		repository.addMerchant(null);
	}
	@Test
	public void givenANewMerchantTryToRegister_merchantAdded() {
		Merchant merchant = new Merchant(new CprNumber("1234"));
		repository.addMerchant(merchant);
		boolean merchantFound = false;
		for(Merchant m:repository.getMerchants()) 
			if(m.equals(merchant)) merchantFound = true;
				
		assertTrue(merchantFound);
	}
	
	@Test
	public void givenARegisteredMerchantTryContains_returnTrue() {
		Set<Merchant> merchants = new HashSet<>();
		merchants.add(merchant);
		repository.setMerchants(merchants);
		assertTrue(repository.contains(merchant));
	}
	
	@Test
	public void givenANonRegisteredMerchantTryContains_returnFalse() {
		assertFalse(repository.contains(merchant));
	}

}
