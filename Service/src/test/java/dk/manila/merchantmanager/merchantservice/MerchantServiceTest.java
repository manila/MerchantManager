package dk.manila.merchantmanager.merchantservice;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import dk.manila.merchantmanager.queue.RabbitMqSender;
import dk.manila.merchantmanager.repository.MerchantRepository;
import dk.manila.merchantmanager.tools.Merchant;

/**
 * 
 * @authorKaloyan, Khushboo
 *
 */
public class MerchantServiceTest {
	private MerchantRepository repository;
	private MerchantServiceImpl service;
	private String cpr;

	@Test
	public void givenCprOfRegisteredMerchant_returnTrue() {
		
		service = new MerchantServiceImpl(new RabbitMqSender("dtu_pay_exchange", "rabbitmq", "fanout"),
				new MerchantRepository() {

					@Override
					public boolean contains(Merchant merchant) {
						return true; 
					}

					@Override
					public void addMerchant(Merchant merchant) {
					}
				});

		assertTrue(service.contains(cpr));
	}

	@Test
	public void givenCprOfNonRegisteredMerchant_returnFalse() {
		service = new MerchantServiceImpl(new RabbitMqSender("dtu_pay_exchange", "rabbitmq", "fanout"),
				new MerchantRepository() {

					@Override
					public boolean contains(Merchant merchant) {
						return false;
					}

					@Override
					public void addMerchant(Merchant merchant) {
					}
				});
		assertFalse(service.contains(cpr));
	}

}
